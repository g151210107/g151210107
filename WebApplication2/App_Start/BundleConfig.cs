﻿using System.Web;
using System.Web.Optimization;

namespace WebApplication2
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/bootsstrap").Include(
                "~/Scripts/bootstrap.min.js",
                        "~/Scripts/plugins.js",
            "~/Scripts/cuntom.js"));

          

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/jquery-1.10.2.min.js",
             "~/Scripts/jquery-migrate-1.2.1.min.js",
            "~/Scripts/modernizr.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/css/bootstrap.css",
                      "~/Content/css/style.css",
             "~/Content/css/font-awesome.min.css",
             "~/Content/animate.css"));
        }
    }
}
